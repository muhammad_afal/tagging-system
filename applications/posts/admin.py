from django.contrib import admin
from .models import Posts, Tags, Images, Like, Dislike

# Register your models here.
admin.site.register(Posts)
admin.site.register(Tags)
admin.site.register(Images)
admin.site.register(Like)
admin.site.register(Dislike)

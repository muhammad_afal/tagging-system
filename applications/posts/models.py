import os
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

User = get_user_model()


# Create your models here.

class TimeStampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Tags(TimeStampModel):

    name = models.CharField(max_length=256, null=True,blank=True)
    weight = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Tags"
        verbose_name_plural = "Tags"
        ordering = ("-weight",)


class Like(TimeStampModel):
    liked_user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.liked_user.username

    class Meta:
        verbose_name = _('Like')
        verbose_name_plural = _('Likes')

class Dislike(TimeStampModel):
    disliked_user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.disliked_user.username

    class Meta:
        verbose_name = _('Dis Like')
        verbose_name_plural = _('Dis Likes')


class Posts(TimeStampModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(_('Title'), max_length=255)
    description = models.TextField(null=True, blank=True)
    hashtags = models.ManyToManyField(Tags, related_name='tags', blank=True)
    likes = models.ManyToManyField(Like, related_name='likes', blank=True)
    dislikes = models.ManyToManyField(Dislike, related_name='dislikes', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')


class Images(models.Model):

    image = models.FileField(
        _("Image"), upload_to="images", null=True, blank=True
    )
    post = models.ForeignKey(
        Posts, on_delete=models.CASCADE, related_name="posts"
    )

    def __str__(self):
        return self.image.name

    def get_file_type(self):
        name, extension = os.path.splitext(self.image.name)
        return extension

    def save(self, *args, **kwargs):
        self.attachment_file_type = self.get_file_type()
        super(Images, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Images"
        verbose_name_plural = "Images"









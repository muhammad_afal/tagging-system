import os
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.http.response import HttpResponse
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view


API_TITLE = 'PostTag API DOC'
API_DESCRIPTION = ''
schema_view = get_swagger_view(title='Swagger API Doc')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('api.urls')),
    url(r'^docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^swagger-docs/', schema_view),
    url(r'^rest-auth/', include('rest_auth.urls'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
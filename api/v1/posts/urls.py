from django.conf.urls import url, include
from rest_framework import routers

from api.v1.posts import views


router = routers.DefaultRouter()
router.register(r'^user-posts', views.UserPost, basename='user-posts')
router.register(r'^like', views.PostLike, basename='like')
router.register(r'^dis-like', views.PostDisLike, basename='dis-like')



urlpatterns = [
    url(r'^', include(router.urls)),
]

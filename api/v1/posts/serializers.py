from rest_framework import serializers
# from drf_extra_fields.fields import Base64ImageField

from applications.posts.models import Like, Posts,Tags,Dislike
from api.v1.accounts.serializers import UserSerializer
# from applications.accounts.models import Crate

from rest_framework.exceptions import APIException
from django.utils.encoding import force_text
from rest_framework import status

from django_currentuser.middleware import (
            get_current_user, get_current_authenticated_user)


class CustomValidation(APIException):
    status_code = status.HTTP_200_OK
    default_detail = 'A server error occurred.'

    def __init__(self, detail, status_code):
        if status_code is not None:self.status_code = self.status_code
        if detail is not None:
            self.detail = {'message':detail, 'status':status_code}
        else: self.detail = {'detail': force_text(self.default_detail)}


class LikesSerializer(serializers.ModelSerializer):
    liked_user = UserSerializer()

    class Meta:
        model = Like
        fields = ['liked_user']
        depth = 0


class DisLikesSerializer(serializers.ModelSerializer):
    disliked_user = UserSerializer()

    class Meta:
        model = Dislike
        fields = ['disliked_user']
        depth = 0


class TagSerializer(serializers.Serializer):

    class Meta:
        model = Tags
        fields = '__all__'
        depth = 0

class ImageSerializer(serializers.Serializer):

    attachment = serializers.FileField()

    def validate_attachment(self,attachment):
        extensions = ('.png','.jpeg','.jpg')
        if not (str(attachment).lower().endswith(tuple(extensions))):
            raise CustomValidation(detail = 'attachment should either be an image', status_code=status.HTTP_400_BAD_REQUEST)
        return attachment

class UserPostSerializer(serializers.Serializer):
    # user = UserSerializer()
    title = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    likes = LikesSerializer(many=True, required=False)
    dislikes = DisLikesSerializer(many=True, required=False)
    tags = serializers.SerializerMethodField(required=False)
    attachements = serializers.SerializerMethodField(required=False)
    likes_count = serializers.SerializerMethodField(required=False)
    dislikes_count = serializers.SerializerMethodField(required=False)
    is_liked = serializers.SerializerMethodField(required=False)
    is_disliked = serializers.SerializerMethodField(required=False)

    def get_title(self, obj):
        return obj.title

    def get_attachements(self, obj):
        urls = []
        for image in obj.posts.all():
            urls.append(image.image.url)
        return urls

    def get_description(self,obj):
        return obj.description

    def get_likes_count(self, obj):
        return obj.likes.all().count()

    def get_dislikes_count(self, obj):
        return obj.dislikes.all().count()

    def get_is_liked(self, obj):
        user = get_current_user()
        liked_list = list(obj.likes.all().values_list('liked_user', flat=True))
        if user.id in liked_list:
            return True
        return False

    def get_is_disliked(self, obj):
        user = get_current_user()
        disliked_list = list(obj.dislikes.all().values_list('disliked_user', flat=True))
        if user.id in disliked_list:
            return True
        return False

    def get_tags(self,obj):
        tags = []
        for tag in obj.hashtags.all():
            tags.append(tag.name)
        return tags


    class Meta:
        model = Posts
        fields = '__all__'
        depth = 0


class PostAddUpdateSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.CharField()
    tags = serializers.ListField(required=True)



class PostLikeSerializer(serializers.Serializer):
    post_id = serializers.CharField()

class PostDislikeSerializer(serializers.Serializer):
    post_id = serializers.CharField()

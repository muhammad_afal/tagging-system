from django.db.models import Count, Q, Case, When

from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from applications.posts.models import Posts, Tags, Like, Dislike
from .serializers import UserPostSerializer, PostAddUpdateSerializer, PostLikeSerializer, \
    PostDislikeSerializer


class UserPost(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'patch', 'delete']
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserPostSerializer
    queryset = Posts.objects.all()

    def list(self, request, *args, **kwargs):
        posts = Posts.objects.all()
        liked_user = Like.objects.get(liked_user=request.user)
        disliked_user = Dislike.objects.get(disliked_user=request.user)
        likes_tag_weight_list = posts.filter(Q(likes=liked_user)).values_list("hashtags__weight")
        dislikes_tag_weight_list = posts.filter(Q(dislikes=disliked_user)).values_list("hashtags__weight")
        posts = posts.annotate(weight=Count('hashtags', filter=Q(hashtags__weight__in=likes_tag_weight_list))).order_by(
            '-weight')
        posts = posts.annotate(weight=Count('hashtags', filter=Q(hashtags__weight__in=dislikes_tag_weight_list))).order_by(
            'weight')
        # posts = posts.order_by(Case(When(hashtags__weight__in=tag_list, then=0), default=1))
        print(posts)
        paginator = PageNumberPagination()
        paginator.page_size = 10
        result_page = paginator.paginate_queryset(posts, request)
        serializer = UserPostSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = PostAddUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        post = Posts()
        post.user = self.request.user
        post.title = request.data.get('title')
        post.description = request.data.get('description')
        post.save()
        for tag in request.data.getlist('tags'):
            hashtag_obj, created = Tags.objects.get_or_create(name=tag)
            post.hashtags.add(hashtag_obj)
        return Response({'message': 'successful', 'status' : status.HTTP_200_OK}, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        post = Posts.objects.get(pk=kwargs['pk'])
        post.hashtags.clear()
        post.product.clear()
        for tag in request.data.getlist('tags'):
            hasgtag_obj, created = Tags.objects.get_or_create(name=tag)
            post.hashtags.add(hasgtag_obj)
            post.save()
        post.title = request.data.get('title', post.title)
        post.description = request.data.get('description', post.description)
        post.save()
        return Response({'message': 'successful', 'status' : status.HTTP_200_OK}, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': 'successful'}, status=status.HTTP_200_OK)


class PostLike(viewsets.ModelViewSet):
    http_method_names = ['post']
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = PostLikeSerializer

    def create(self, request, *args, **kwargs):
        serializer = PostLikeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        post = Posts.objects.get(pk=request.data.get('post_id'))
        like, created = Like.objects.get_or_create(liked_user=user)
        post.likes.add(like)
        dis_likes = post.dislikes.all()
        for dis_like in dis_likes:
            if dis_like.disliked_user == like.liked_user:
                post.dislikes.remove(dis_like)
        post.save()
        return Response({"username" : user.username, "first_name" : user.first_name, "last_name" : user.last_name, "email" : user.email, "id" : like.id}, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': 'successful'}, status=status.HTTP_200_OK)


class PostDisLike(viewsets.ModelViewSet):
    http_method_names = ['post']
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = PostDislikeSerializer

    def create(self, request, *args, **kwargs):
        serializer = PostDislikeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        post = Posts.objects.get(pk=request.data.get('post_id'))
        dis_like, created = Dislike.objects.get_or_create(disliked_user=user)
        post.dislikes.add(dis_like)
        likes = post.likes.all()
        for like in likes:
            if like.liked_user == dis_like.disliked_user:
                post.likes.remove(like)
        post.save()
        return Response({"username" : user.username, "first_name" : user.first_name, "last_name" : user.last_name, "email" : user.email, "id" : dis_like.id,}, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({'message': 'successful'}, status=status.HTTP_200_OK)






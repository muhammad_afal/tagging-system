from rest_framework import permissions


class OTPVerifyPermission(permissions.BasePermission):
    message = 'Please verify email'

    def has_permission(self, request, view):
        return request.user.email_verified

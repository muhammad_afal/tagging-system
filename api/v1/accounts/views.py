import random
import re
import string
import os
import base64
import requests
from django.db.models import Q
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.conf import settings

from rest_framework.response import Response
from rest_framework import viewsets, status, permissions
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from .serializers import RegisterSerializer, UserLoginSerializer
from applications.posts.models import Posts


User = get_user_model()


class UserRegistrationView(viewsets.ModelViewSet):
    http_method_names = ['post', ]
    serializer_class = RegisterSerializer

    def create(self, *args, **kwargs):
        serializer = self.serializer_class(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.data.get('username')
        user = User.objects.create(username=username)
        user.set_password(serializer.data.get('password'))
        user.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response(data={'message': 'User succesfully created', 'token': token.key, 'id': user.id, 'status':status.HTTP_200_OK},
                        status=status.HTTP_200_OK)


class UserLoginView(viewsets.ModelViewSet):
    permission_classes = (permissions.AllowAny,)
    http_method_names = ['post']
    serializer_class = UserLoginSerializer

    def create(self, *args, **kwargs):
        serializer = self.serializer_class(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        username = User.objects.get(username=serializer.data.get('username'))
        pwd = serializer.data.get('password')
        user = authenticate(username=username, password=pwd)
        if not user:
            return Response({'message': 'Invalid credentials'},
                            status=status.HTTP_401_UNAUTHORIZED)
        token, created = Token.objects.get_or_create(user=user)
        return Response(data={'message': 'Success', 'token': token.key,
                              'id': user.id,
                                'is_email_verified':user.email_verified,
                             },
                        status=status.HTTP_200_OK)




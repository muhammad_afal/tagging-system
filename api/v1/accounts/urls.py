from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from rest_framework import routers

from api.v1.accounts import views


router = routers.DefaultRouter()
router.register(r'^register', views.UserRegistrationView, basename='register')
router.register(r'^login', views.UserLoginView, basename='login')

from django.urls import path
from django.contrib.auth import get_user_model

User = get_user_model()

urlpatterns = [
    url(r'^', include(router.urls)),
    # url(r'^logout/', views.Logout.as_view()),
]
# ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

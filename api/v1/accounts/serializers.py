import re

from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from rest_framework.exceptions import APIException
from django.utils.encoding import force_text
from rest_framework import status


User = get_user_model()

class CustomizedValidation(APIException):
    status_code = status.HTTP_200_OK
    default_detail = 'A server error occurred.'

    def __init__(self, detail, status_code):
        if status_code is not None:self.status_code = self.status_code
        if detail is not None:
            self.detail = {'message':detail, 'status':status_code}
        else: self.detail = {'detail': force_text(self.default_detail)}

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
        depth = 0

    def validate_username(self, username):
        if User.objects.filter(Q(username=username)).exclude(username=self.context.get('request').
                user.username).exists():
            raise serializers.ValidationError('Username already exist')
        return username


class RegisterSerializer(serializers.ModelSerializer):
    pass

    class Meta:
        model = User
        fields = '__all__'
        depth = 0


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate_username(self, username):
        if not User.objects.filter(username=username).exists():
            raise serializers.ValidationError("user not registered")
        return username



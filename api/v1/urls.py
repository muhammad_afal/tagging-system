from django.conf.urls import url, include


urlpatterns = [
    url(r'^accounts/', include('api.v1.accounts.urls')),
    url(r'^posts/', include('api.v1.posts.urls')),
]
